#include "sudokuMachine.h"

SudokuMachine::SudokuMachine()
{
	size = 9;
}

bool SudokuMachine::setSudokuSize(int s)
{
	size = s;
	return true;
}

bool SudokuMachine::loadSudokuMatrix(int**& smp)
{
	sudokuMatrixToSolve = smp;
	return true;
}

int** SudokuMachine::createSudokuMatrix(int matrixSize)
{
	int** matrix = nullptr;
	matrix = new int*[matrixSize];	
	for (int i = 0; i < matrixSize; i++) {
		matrix[i] = new int[matrixSize];
		memset(matrix[i], 0, sizeof(int)*matrixSize);
	}
	return matrix;
}

bool SudokuMachine::solveSudokuMatrix()
{
	sudokuMatrixSolved = createSudokuMatrix(size);
	std::vector<int> rowHoles = getRowHoles();
	std::vector<int> columnHoles = getColumnHoles();
	std::vector<int> squareHoles = getSquareHoles();



	return 0;
}

SudokuMachine::~SudokuMachine()
{
}

std::vector<int> SudokuMachine::getRowHoles()
{
	std::vector<int>rowH;
	for (int i = 0; i < size; i++) {
		
		int rowHole = 0;

		for (int j = 0; j < size; j++) {
			(sudokuMatrixToSolve[i][j] == 0) ? rowHole++ : __noop;
		}
		rowH.push_back(rowHole);
	}
	return rowH;
}

std::vector<int> SudokuMachine::getColumnHoles()
{
	std::vector<int>columnH;
	for (int i = 0; i < size; i++) {

		int columnHole = 0;

		for (int j = 0; j < size; j++) {
			(sudokuMatrixToSolve[j][i] == 0) ? columnHole++ : __noop;
		}
		columnH.push_back(columnHole);
	}
	return columnH;
}

std::vector<int> SudokuMachine::getSquareHoles()
{
	std::vector<int>squareH;

}