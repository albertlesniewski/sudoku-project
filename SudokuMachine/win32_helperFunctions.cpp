#include "win32_helperFunctions.h"

using namespace std;

void printSudoku(int**& mtrx, int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			cout << mtrx[i][j] << " ";
		}
		cout << "\n";
	}
}

void fillSudokuFromFile(int **& mtrx, ifstream & inn, int size)
{
	char row[10];
	for (int i = 0; i < size; i++) {
		inn >> row;
		for (int j = 0; j < size; j++) {
			mtrx[i][j] = row[j] - '0';
		}
	}
}
