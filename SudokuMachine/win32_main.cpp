#include <iostream>
#include <fstream>

#include "sudokuMachine.h"
#include "win32_helperFunctions.h"

using namespace std;

int main() {

	SudokuMachine sm;

	int** sudokuMatrix = sm.createSudokuMatrix(9);
	ifstream inn("expSudoku.dat");
	fillSudokuFromFile(sudokuMatrix, inn, 9);
	printSudoku(sudokuMatrix, 9);
	sm.loadSudokuMatrix(sudokuMatrix);

	sm.solveSudokuMatrix();


	return 0;
}