#pragma once
#include <string.h>
#include <vector>
//#include <iostream>

class SudokuMachine
{
public:
	SudokuMachine();

	bool setSudokuSize(int s);
	bool loadSudokuMatrix(int**& smp);

	//Return pointer to sudoku matrix
	int** createSudokuMatrix(int matrixSize);
	bool solveSudokuMatrix();


	~SudokuMachine();

private:
	int size;
	int** sudokuMatrixToSolve;
	int** sudokuMatrixSolved;

	std::vector<int> getRowHoles();
	std::vector<int> getColumnHoles();
	std::vector<int> getSquareHoles();

};