#pragma once
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

void printSudoku(int**& mtrx, int size);
void fillSudokuFromFile(int **& mtrx, ifstream & inn, int size);
